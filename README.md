<br>
<br>

---

# Transformative AI Deployment Challenge

<br>

## Introduction

Welcome Omdena participant! The aim of this challenge is to take the machine-learning pipeline for CodeRhythm developed by Transformative AI and deploy it on a cloud service. This should be query-able through an API such that a user (using a mobile app, for example) should be able to send data to the API and receive back the model's prediction, which will then be handled by a separate front-end team and displayed on the app.

Specifically, the tasks are:

- Draft a reference architecture for real-time input/output handling.
- Develop a model API using Flask or similar.
- Containerise the API as a microservice using Docker or similar.
- Deploy the model API to the web using AWS Elastic Beanstalk, Heroku, GCP App Engine, or similar.

In the rest of this document, we will go over the work that has already been done by Transformative AI, including the pipline for generating the date and predictions, as well as the structure for handling multiple concurrent patients in a live scenario.

<br>

---

<br>

## Important note about project flexibility

We want to emphasise that many of the guidelines set out here do not comprise hard-and-fast rules. If there is some system or structure in this existing python code which you believe should be changed, then you may do so (or contact us to do so if that is more suitable). Additionally, if you believe that any of the tasks listed above should be changed to better suit the overall aim stated at the top of this document, do not hesitate to reach out to us! We can be reached at:

- Marek, CTO: sirendi@transformative.ai
- Diogo, Senior Data Scientist: santos@transformative.ai
- Scott, Data Scientist: vinay@transformative.ai

<br>

---
<br>

## Overview of the existing pipeline

CodeRhythm is a product which takes a segment of electrocardiogram (ECG) data from a patient in a hospital and returns a prediction - a single number between 0 and 1 that represents the risk of that patient having a ventricular tachyarrhythmia within the next few hours. Each prediction is based on several minutes worth of data. This data is passed through a number of algorithms (in the generate folder) before being returned.

One key thing to note is that while 10 second segments of data are sent in each packet to the algorithm, this algorithm requires those 10 seconds *in addition to* the few minutes before that in order to make a prediction. Therefore, we need to hold on to the most recent few minutes of data for each patient. This is handled by 
Patient class as described below.

<br>

---

<br>

## Setting up the repo

### Downloading supplemental data

The folders `models` and `sample_data` are not included in the remote repo due to their large size. They can be downloaded from https://www.dropbox.com/sh/n1ud1a9zvk7sm73/AACrEuEBP0JBevEs5EkBnZWla?dl=0. This can be done from the command line with `wget [link]` followed by `unzip [downloaded filename]`. You will need to install unzip first with `sudo apt install unzip`. The two folders contained within should be placed within the root directory of the repo (`live_interface`). Note that these are both included in `.gitignore`.

<br>

### Installing required modules
The file `environment.yml` contains a list of Python packages that were installed for this code. You can either install these manually, or (recommended) create a new conda environment that installs them all, using `conda env create -f environment.yml`. This is confirmed to work on Linux.

<br>

---

<br>

## Files and folders in this repo

The files here are covered in the general logical order of importance. It is intended that the structure will make most sense if you read this section in order. For more detail see the comments and docstrings within the files themselves.

<br>

### `interface.py`

- The `main` section (i.e. the block beginning `if __name__=='__main__'`) is intended as an example piece of code for how machines in an ICU may pass data to the rest of the code base. A list of patients is created (`records`) and passed to the persistent object `handler` which contains a number of `Patient` objects.
- The format of the `packet` object here is the expected format for data sent to an instance of the `Handler` class via the `update_and_predict` method. It should contain
    - `patientID` (string)
    - `fileID` (string. Can be empty.)
    - `admissionID` (string. Can be empty.)
    - `lead` (string. Will generally be `"II"` in practice.)
    - `fs` (integer. Sampling rate. Will generally be `250` in practice.)
    - `data` (list of floats. This can be of any length, but it is intended to be 10 times fs, representing 10 seconds of data.)
    - `time` (string. Time of the first point in data. Given in American format of date then time including seconds. For the second of January 2020, at 3pm this will be `01/02/2020 15:00:00`.)

<br>

### `Handler` (class in `handler.py`)
- An instance of this object remains persistent in the program runtime. However, you may find it most useful to modify, restructure, or even scrap this approach if it does not fit with the best way of doing things in your chosen framework
- `update_and_predict` - This is the method for external interfacing with this class. It takes two arguments. The first, `jsondata`, is a json file (or Python dictionary) of the format described above. The latter, `mode`, determines the output format. This is by default `pandas`, which gives a pandas dataframe. However, this may be set to `basic`, which returns two lists: `dtstrings` and `risks`. The first, `dtstrings`, is a list of times, and `risks` is a list of predictions at those times. When passed a new packet, it will find the right `Patient` object, or create a new one and add it to `self.patients`.
- The models are stored as attributes of this class. Some are stored in `self.featuremodels`, which are used for generating the features. A model is also stored in `self.finalmodel`, which takes in a tensor of features and produces a prediction.

<br>

### `Patient` (class in `patient.py`)

- This contains a list, `self.signals`, a list of instances of the `Signal` class, one for each `fileID`, `admissionID` combination.
- `update_pat` - This is referenced by `Handler`. It creates a new `Signal` if needed, or updates an existing one if not.

<br>

### `Signal` (class in `patient.py`)

- The key attributes here are `self.times`, `self.data`, and `self.new_report_times`. The attribute `self.times` is the list of all times for which we have an ECG point. the attribute `self.data` is that list of ECG data points. This should be of the same length as `self.times`. The attribute `self.new_report_times` is the set of times for which feature sets are passed back up to an instance of `Patient` and then `Handler`.
- `update_sig` - This takes the new data packet from `Handler` -> `Patient` and adds times and data to `self.times` and `self.data`. If there is a gap in `self.times`, the missing times are added and the corresponding data points are set to NaN. If there is an overlap, the data and times are not duplicated.
- `chop_time_data` - This method is called from `update_sig`. It chops both `self.times` and `self.data` such that no more than `memtime` seconds are kept, where `memtime` is defined by the entry `memory_time` in the config file as described below. 


### `live_config.json`

- This is the config file, which is loaded within `interface.py` and is passed to `Handler` -> `Patient` -> `Signal`. The path variables can be changed if needed. All other options should generally be left as the default. The keys are as follows:

<br>

- `shapes_group_path` (string) - This is the path to the feature model for the "Shapes" algorithm. Default is `"models/fextract_16.pkl"`.
- `cnn_model_path` (string) - This is the path to the feature model for the "CNN" algorithm. Default is `"models/cnn_best.h5"`.
- `finalmodel_type` (string) - The type of model that is used for the final model. At the moment, only `keras` is accepted here.
- `finalmodel_type` (string) - This is the path to the final model folder. Default is `"models/m1_0"`.

<br>

- `sample_data_path` (string) - Path to the folder containing ECGs which are loaded by `interface.py`. Note that these are in the format recognised by the WFDB library (installed by `environment.yml`). This folder is not contained in the repo for size reasons. See the section `Setting up the repo` -> `Downloading supplemental data` above. Default is `sample_data`.
- `feature_list_path` (string) - List of features in order used by the final model. Default is `"features_list.txt"`.

<br>

- `algorithms` (list of strings) - The list of algorithms which are called to produce features. Default is `["ecg","ecg_lookback_120","shapes_16","cnn","rr"]`

<br>

- `memory_time` (int) - The time in seconds for which data is retained in an instance of `Signal`. Default is `600`.
- `seconds_in_sample` (int) - How many seconds of data to pass to a Handler object at a time. Default is `10`.
- `step` (int) - The time interval for between risk reports. Default is `10`.
- `lockstep` (bool) - Whether or not reporting times are all rounded down to be on integer intervals of the `step` value. Default is `True`.

<br>

- `ecg_window` (int) - Window of time used to produce "ECG" features. Default is `10`.
- `rr_beat_window` (int) - Number of beats used to produce "RR" features. Default is `240`.
- `shapes_window` (int) - Window of time used to produce "Shapes" summary-features from raw features. Default is `10`.
- `cnn_window` (int) - Window of time used to produce "CNN" summary-features from raw features. Default is `10`.

<br>

- `stack_size` (int) - Number of beats used to produce "CNN" features. Default is `128`.
- `cnn_batch_size` (int) - Batch size for the "CNN" feature model. Default is `64`.
- `cnn_batch_jump` (int) - Internal parameter used for the "CNN" feature model. Default is `100000`.

<br>

## The changes are made for the deployment
<br>

### `live_config.json` 
- `bucket_name` (string) - Name of bucket storage to backup the raw data.
- `topic_name` (string) - Name of topic to receive the data pusblished by web application.
- `subscriber_1` (string) - Name of first subcription to pull data from Topic and send data to Bucket.
- `subscriber_2` (string) - Name of second subscription to pull data from Topic and send data to Database.
- `project_id` (string) -  ID of project created on Google Cloud.
- `patientscache` (string) - Name of Bucket to cache history data of patients.
- `instance_id` (string) - Name of Bigtable instance.
- `table_id` (string) - Name of table in Bigtable.

<br>
### `service-account-keys.json` 
- A file contains the private keys and the role of user to access gcloud resources.

<br>
### `requirements.txt`
- a list of opensource libraries required to be installed.

<br>
### `app.yaml`
- Act as a descriptor for App Engine deployment
- Specifies python version and Python Web Server Gateway Interface (WSGI) HTTP server. In this case, we use Gunicorn as web server.

<br>
### `main.py`
- A main file to start the application to Google App Engine.
- Implement a restful post api request to be able to receive streaming data from users through http request. 
- Implement the concurrency of data processing and predictions. 
- Play a role as a publisher in Pubsub model to publish data to Topic.
- Get the CA score from final model and send it back to end-user and save it into the database.
- In the backgroud, a process of caching history data of patients as pickle object to bucket. This operation is runing indepently with other running processes.

<br>
### `starter.py` (in /pusub folder)
- An iterface to manipulate the operations of Google Storage and Google Pubsub.
- Create buckets/topics/subscripts if they are not existed yet when the application is started.
- Send the data to Topics, Pull data from subcriptions and cleanup the old data.
- Delete files on Bucket.

<br>
### `simulation.py` (in /pusub folder)
- The code is mainly ported from `interface.py`
- Serve for testing purpose: sending the patient data to web app and recieve the result.
<br>
### `bgtstore.py` (in /pusub folder)
- An interface of data manipulation from BigTable.
- Create big table instance, table for storing patient data.
- Insert_data is used to store the received raw packet.
- Update_scores is called to update the scores and timestamps list for each patientId.

<br>
### `Dockerfile`
- Create an image docker from the application.
- Automatically build and start the application with Docker image.

##  Deploy the application to local VM (Virtual Machine) and to Google App Engine
- Check out the souce code on the repository: `git clone <repo_link>`
- Download the latest models available on Dropbox: `wget <dropbox_link> --content-disposition` and unzip the compressed file: `sudo apt install unzip`
- Install required models: conda env create -f environment.yml
- Install extra python libraries: `pip install -r requirements.txt`
- Deploy and start the app:
    - Local VM: `uvicorn main:app --reload`
    - App Engine (with gcloud): gcloud app deploy app.yaml

## Create Docker image on VM:
- Install the Docker for Debian: https://docs.docker.com/engine/install/debian.
- Create the image: `sudo docker build -t <image name>`.
- Run the image: `sudo docker run  -p <port>:<port> -h <ip host> -it <name of image>`. For example: `sudo docker run  -p 8000:8000 -h 127.0.0.1 -it livedepimage`.
- List the images: `sudo docker images`
## Useful commands to manipulate Gcloud Storage, VM memory and App Engine log
- Cleanup the bucket storage: `gstuil -m rm gs://<bucket name>`. Don't forget to install gsutil: `sudo apt install -y cloud-utils`.
- View server log: gcloud app logs --tails
- There is an space issue when application is started running. It requires more memory to store the patient history/
    - List the available space on filesystem: df -h
    - Resize the space of VM: `gcloud compute disks resize <VM name> --size <disk size>`. 
    - Resize the partition of filesystem: `sudo resize2fs /dev/sda1`.


<br>
### Others
- `.gitignore` - Folders and filetypes to be ignored by git. The models and sample_data folders are referenced here.
- `models/` (folder) - Contains model files. Not included in repo. See section `Setting up the repo`.
- `sample_data/` (folder) - Contains data files. Not included in repo. See section `Setting up the repo`.
- `__init__.py` - Allows folder to be loaded as a package.
- `environment.ylm` - Used for setting up a conda environment. See section `Setting up the repo`.
- `utils.py` - General utility functions.
- `generation/` (folder) - Contains files for calling the different algorithms used by `Signal.get_features`.
- `rr_library/` (folder) - Various files for the "RR" algorithm.
- `signallab/` (folder) - Package for the "ECG" algorithm.