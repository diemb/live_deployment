# -*- coding: utf-8 -*-
import numpy as np
import cmath
import random
#import numba

#@numba.jit
def ectopic_cleaning_v2(rr_intervals):
    """
    """
    
    X = rr_intervals.copy()
    n = X.size

    t = np.zeros(n)

    for N in range(0,2):
        
        t[0] = (X[0]+6*X[0]+15*X[0]+20*X[0]+15*X[1]+6*X[2]+X[3])/64                                                               
        t[1] = (X[1]+6*X[1]+15*X[0]+20*X[1]+15*X[2]+6*X[3]+X[4])/64
        t[2] = (X[2]+6*X[1]+15*X[0]+20*X[2]+15*X[3]+6*X[4]+X[5])/64

        for i in range(3,n-3):
            t[i] = (X[i-3]+6*X[i-2]+15*X[i-1]+20*X[i]+15*X[i+1]+6*X[i+2]+X[i+3])/64

        t[n-3] = (X[n-6]+6*X[n-5]+15*X[n-4]+20*X[n-3]+15*X[n-2]+6*X[n-1]+X[n-3])/64
        t[n-2] = (X[n-5]+6*X[n-4]+15*X[n-3]+20*X[n-2]+15*X[n-1]+6*X[n-2]+X[n-2])/64
        t[n-1] = (X[n-4]+6*X[n-3]+15*X[n-2]+20*X[n-1]+15*X[n-1]+6*X[n-1]+X[n-1])/64

        Ua = np.zeros(n)
        lampda = np.zeros(n)
        Ua[0] = X.mean()
        lampda[0] = Ua[0]**2

        c = 0.05
      
        for i in range(1,n):
            Ua[i] = Ua[i-1] - c*(Ua[i-1] - t[i-1])
            lampda[i] = lampda[i-1] - c*(lampda[i-1] - t[i-1]**2)
        
        sigma = np.zeros(n)
        for i in range(0,n):
            sigma[i] = abs(cmath.sqrt(Ua[i]**2-lampda[i]))
        
        Cf = 3
        p = 10
        sigma_mean = sigma.mean()
        last = Ua[0]
        
        for i in range(1,n):
            if abs(X[i]-X[i-1]) > p*X[i-1]/100+Cf*sigma_mean and abs(X[i]-last) > p*last/100+Cf*sigma_mean:
                X[i] = random.random()*sigma[i]+(Ua[i]-0.5*sigma[i])
            else:
                last = X[i]

    Cf1 = 3
    sigmaB = 0.02

    f_hrv = X.copy()
    counter = 0

    for i in range(0, n-1):
	    if abs(X[i]-Ua[i]) > (Cf1*sigma[i]+sigmaB):
		    f_hrv[i] = t[i]
		    counter += 1

    return f_hrv, X, float(counter)/len(rr_intervals)