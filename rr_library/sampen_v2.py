# -*- coding: utf-8 -*-
import ctypes
from ctypes import cdll
import numpy as np
import os

def get_sampen(pyarr, mm, r):
    
    libsampen = cdll.LoadLibrary('rr_library/sampen_physio_ubuntu_ws.so')
    
    pyarr -= np.mean(pyarr)
    pyarr /= np.std(pyarr)
    
    seq = ctypes.c_double * len(pyarr)
    arr = seq(*pyarr)
    
    MM = ctypes.c_int(mm)
    
    R = ctypes.c_double(r)
    
    N = ctypes.c_int(len(pyarr))
    
    zeros = np.zeros(4,)
    res = ctypes.c_double * 4
    res = res(*zeros)
    
    libsampen.sampen(arr, MM, R, N, res)

    return res