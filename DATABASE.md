## Database Implementation and How to use

### Introduction <br/>

The cloud bigtable is a highly sparse table that scales up to thousands of columns and billions of rows, this enables storing of terabytes or petabytes of data.
Every value in a row is indexed, and the values are known as row key. Bigtable possess low latency, high throughput for read and write processes[1]
The table in bigtable contains rows, each row is referred to as an entity and the column contains distinct values for every row.
Individual rows are indexed by a row key, and columns that possess a relationship are grouped together and referred to as column family.
Individual columns are identified through the combination of column family and column qualifier. A column qualifier is a unique name within the column family.[1]

### BigTable Components and Structure

The bitable is made up of four major components: bigtable instance, bigtable table, bigtable column, and big column family.
Bigtable instance: Our bigtable instance name is 'webapp-instance' located in the 'US' region. 
NB: When querying the data from bigquery, the region where the bigtable resides is very important. 
The instance has '10 nodes', each takes care of requests and multiple nodes handle multiple requests simultaneously. 
The instance has a size of '25TB', which can be increased if low memory is encountered.

Bigtable table: The bigtable name is patientdata-table with two column families.

Bigtable column family: The column families are:- ‘patData’ and ‘patScores’. patData is used to store the raw patient data and patScores is used to store the model score returned by the model.

Database Table Structure: The column family name ‘patData’ stores the following raw data:- fileID, fs, lead, patient data, and time. 

Furthermore, the patientId and admissionID forms the row key. Meaning the data can be filtered using either the ‘patientId’ or ‘admissionId’ in the row key. 
The row key is used to store both the raw data(patData) and the model result(patScore). 
The column family named ‘patScores’, consist of the following columns:- timestamp and score. This data is stored using the same row key as ‘patData’.

### Database processes and examples

How to create, delete, new table or instance

=> To create a new table: <br/>

1. Go to the ‘release branch’ and inside the pubsub folder. 

2. Locate bgtstore.py

3. Uncomment the create_table() method and enter the name of the new table and run. Or call the method from another function.

=> To delete an existing table: <br/>

1. Inside the bgtstore.py; uncomment the delete_table() method and type the name of the table to be deleted into the method. Then, run the script.

=> These steps can be repeated to create an instance and delete an instance.  

How to query the database: via python

There is a script(bgtread.py) in the release branch for retrieving stored value in the bigtable. 
The values in the bigtable can be restored using a prefix or the row key to retrieve the latest entry into the database.

=>To read data from the table: <br/>

1. Go to the ‘release branch’ and inside the pubsub folder

2. Locate the bgtread.py

3. Call the function ‘read_latest_row’ to read the latest data stored on the bigtable. And call the function ‘read_prefix’ to read all the data on the bigtable based on the prefix.

=> Examples:

1. To read the latest data entry, call ‘read_latest_row’. Ensure to enter the ‘tableID’

2. To read the entire rows based on the prefix selected from the key, run the function as shown below:

===> data_rows = read_prefix('SDDB_31') 

===> for row in data_rows:

====>    print_row(row)

For more info about the database, follow this link: https://docs.google.com/document/d/1attby3Y16nolqJ2f3zIF8xDN12Y1XkkVdOLDI6UdsaA/edit?usp=sharing

### Reference

1. https://cloud.google.com/bigtable/docs/overview