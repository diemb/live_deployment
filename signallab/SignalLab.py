#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 15:12:16 2019
@author: diogosantos
"""

import pickle
import numpy as np
import matplotlib.pyplot as plt

import pandas as pd

import pywt
from wfdb.processing.qrs import xqrs_detect, gqrs_detect

# import data
# import tools

import multiprocessing as mpc
from functools import partial
import time

from scipy import signal as spsig
from collections import defaultdict

import sys
import os

from . import tools


# Keys to processed signals
SIGNAL_PROCESS_KEY = {'raw': 'signal_raw',
                      'filtered': 'signal_filt',
                      'standardized': 'signal_std',
                      'cwt': 'signal_cwt',
                      'swt': 'signal_swt',
                      'uwt': 'signal_uwt'}

class Signal:
    """
    Base signal class
    --- INPUT: ---
    singal_raw (numpy array): 1D raw signal
    fs (int): smapling frequency
    category (str): defines the type of signal to load
    --- RETURN: ---
    Signal (object):
    """
    def __init__(self, signal_raw=None, fs=None, category=None, verbose=False):

        # if the singal catefory has its own subclass
        if category == None:
            self.category = 'generic'
        else:
            self.category = category
        if verbose:
            print('Signal category assigned to', self.category)

        if signal_raw is None:
            self.signal_raw = []
            self.fs = []
            self.len =[]

        else:
            self.signal_raw = signal_raw
            self.fs = fs
            self.len = len(signal_raw)

    def __repr__(self):
        return "Signal('{}',{})".format(self.signal_raw, self.fs)

    def __str__(self):
        return 'To-do'

    def __add__(self, other):
        """
        Adds two Signal objects with similar category and sampling frequency
        --- INPUT: ---
        other: second signal obejct to add
        --- RETURN: ---
        Singal (object): new Signal object with both signals concatenated
        """
        try:
            if self.fs == other.fs and self.category == other.category:
                temp = np.hstack((self.signal_raw, other.signal_raw))
            return Signal(temp, self.fs, self.category)
        except Exception as e:
            print(e)
            print('Signals must have similar FS and TYPE')

    def __len__(self):
        return self.len

    def load(self, fmt):
        if fmt == 'mimic2':
            self.signal_raw, self.fs, self.len, self.alarms = data.load.mimic2()
        elif fmt == 'mimic3':
            self.signal_raw, self.fs, self.len = data.load.mimic3()
        elif fmt == 'ajou':
            self.signal_raw, self.fs, self.len = data.load.ajou()
        elif fmt == 'nmec':
            self.signal_raw, self.fs, self.len = data.load.nmec()
        else:
            raise ValueError('Format not currently suported')

    def length(self, unit=None):
        """
        Converts the signal lenght from samples to seconds, minutes or hours
        --- INPUT: ---
        unit (str): time units for convertion, default is samples
        --- RETURN: ---
        self.length (int): singal length in the desired units
        """
        if isinstance(unit, str):
            if unit is None or unit == 'samp':
                return self.len
            if unit == 'sec':
                return self.len/self.fs
            if unit == 'min':
                return self.len/self.fs/60
            if unit == 'h':
                return self.len/self.fs/60/60
            raise ValueError('input "samp" for samples, "sec" for seconds,\
                             "min" for minutes and "h" for hours')
        else:
            raise TypeError('input "samp" for samples, "sec" for seconds,\
                            "min" for minutes and "h" for hours')

    def filtering(self, signal_flag='raw', **kwargs):
        """
        Applies an Nth-order digital Butter-worth zero-phase digital filter
        and returns the filtered signal
        --- INPUT: ---
        signal_flag (str): define which signal to load, default is raw
        order (int): order of the filter
        cutoff (list of ints): cut-off frequencies for the filter
        btype (str): type of filter to used
        --- RETURN: ---
        self.signal_filt (numpy array): signal filtered with selected parameters
        """
        if signal_flag not in SIGNAL_PROCESS_KEY:
            raise ValueError('Unknown signal_flag')

        sig = getattr(self, SIGNAL_PROCESS_KEY[signal_flag])

        #filters the signal using a zero-phase digital filter
        self.signal_filt, self.filt_set = tools.tools.filtering(sig, self.fs, **kwargs)

    def normalisation(self, mean=None, std=None, signal_flag='raw', verbose=False):
        """
        Performs signal normalisation
        --- INPUT: ---

        --- RETURN: ---
        """
        if verbose:
            print('performing: normalisation')

        if signal_flag not in SIGNAL_PROCESS_KEY:
            raise ValueError('Unknown signal_flag')

        sig = getattr(self, SIGNAL_PROCESS_KEY[signal_flag])

        self.singal_normal, self.normalisation_values = tools.tools.normalise(sig, mean, std)

        if verbose:
            print('finished: normalisation')

    def cwt(self, signal_flag='raw', scale=[2,4,8,16], wavelet='mexh'):
        """
        Performs Continuous Wavelet Transform (CWT)
        --- INPUT: ---
        --- RETURN: ---
        """
        print('running: cwt')
        if signal_flag in SIGNAL_PROCESS_KEY:
            self.cwt_coefs, self.cwt_freqs = pywt.cwt(
                    getattr(self, SIGNAL_PROCESS_KEY[signal_flag]), scale,
                    wavelet)
            self.cwt_scale = scale
        else:
            raise ValueError('Unknown signal_flag')

    def swt(self, signal_flag='raw', wavelet='coif1', level=5,
            start_level=0, axis=-1):
        """
        Performs Stationary Wavelet Transform (SWT)
        --- INPUT: ---
        --- RETURN: ---
        """
        print('running: swt')
        if signal_flag in SIGNAL_PROCESS_KEY:
            self.swt_coefs = pywt.swt(
                    getattr(self, SIGNAL_PROCESS_KEY[signal_flag]), wavelet,
                    level, start_level, axis)
            self.swt_level = level
        else:
            raise ValueError('Unknown signal_flag')

    def show(self, signal_flag='raw', new_fig=True, start=None, end=None,
             xlim=None, ylim=None, legend=False):
        """
        Plot signal
        --- INPUT: ---
        --- RETURN: ---
        """
        plt.ion()
        if new_fig is True:
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)

        ax.plot(self.time, getattr(self, SIGNAL_PROCESS_KEY[signal_flag]), label='signal')
        '''
        if rr == True:
            ax.plot(self.qrs_idx/self.fs, getattr(self, SIGNAL_PROCESS_KEY[signal_flag])[self.qrs_idx],
                    'rx', label='R-wave index')
        if rr_ext == True:
            ax.plot(self.rr_ext, getattr(self, SIGNAL_PROCESS_KEY[signal_flag])[(self.rr_ext*self.fs).astype(int)],
                    'rx', label='R-wave index (external)')
        '''
        if legend is True:
            ax.legend()

        fig.tight_layout()
        plt.show()

    def save_to_csv(self, attribute, loc, name):
        """
        Exports data to CSV files
        --- INPUT: ---
        --- RETURN: ---
        """
        if hasattr(self, attribute):
            np.savetxt(loc+name, getattr(self, attribute), delimiter=",", fmt='%s')

    def save_to_pickle(self, file_name):
        """
        Exports data to pickle files
        --- INPUT: ---
        --- RETURN: ---
        """
        with open(file_name, 'w') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

# dictionary representing the features name and size
ECG_FEATURES = {'stats': 11,
                'tci': 2,
                'tcsc': 2,
                'std_exp': 1,
                'mod_exp': 1,
                'mva': 1,
                'count': 3,
                'x_time': 2,
                'bcp': 1,
                'vfleak': 1,
                'spec': 4,
                'x_freq': 3,
                'bwt': 1,
                'li': 1,
                'jekova': 4,
                'p_sqi': 1,
                'bas_sqi': 1,
                'snr': 1}

FEATURE_NAMES = [
        # Stats
        'mean','std','median','iqr','max','max_abs','min','var','rms','kurt','skew',#TODO
        # TCI
        'tci_mean','tci_sd',
        # TSCS
        'tcsc_mean','tcsc_sd',
        #
        'std_exp','mod_exp','mva',
        # Count
        'count1','count2','count3',
        # X_time
        'x1','x2',
        #
        'bcp','vfleak',
        # Spec
        'm','a1','a2','a3',
        # X_freq
        'x3','x4','x5',
        #
        'bwt','li',
        # Jekova
        'cv_bin','frq_bin','area_bin','kurt_bin',
        #
        'p_sqi','bas_sqi','snr'
        ]

# This gets the feature columns by ECG_FEATURES name. Needed for 
# getting dataframe columns by self.features.
FEATURE_GROUPS = {}
totalf = 0
for f in ECG_FEATURES.keys():
    f_in_group = ECG_FEATURES[f]
    FEATURE_GROUPS[f] = FEATURE_NAMES[totalf:totalf+f_in_group]
    totalf += f_in_group

class ECG(Signal):
    """
    ECG signal class
    """
    def __init__(self, signal_raw, fs, lead=None, fmt=None, time=None, rr_ext=None):
        super().__init__(signal_raw, fs, category='ECG')
        if lead is None:
            self.lead = 'ECG'
        else:
            self.lead = lead

        if fmt is None:
            self.fmt = 'unknown'
        else:
            self.fmt = fmt

        if time is None:
            self.time = np.arange(0, self.len/self.fs, 1/self.fs)
        else:
            self.time = time

        if rr_ext is None:
            self.rr_ext = []
        else:
            self.rr_ext = rr_ext

    '''def add_philips_ann(self, philips_ann_raw):
        """
        Converts ECG annotations from Philips ECG device
        """
    '''

    def qrs_detector(self, detector='drs', correct_peak=True):
        """
        QRS complex detection algorithm
        --- INPUT: ---
        --- RETURN: ---
        """
        print('performing: QRS complex detection')
        if detector == 'drs':
            if not hasattr(self, 'signal_filt'):
                self.filtering()
            if not hasattr(self, 'cwt_coefs'):
                self.cwt(signal_flag='filtered', scale=4)
            self.qrs_idx = tools.ecg_qrs_drs.detect(self.cwt_coefs[0], self.fs)
        elif detector == 'xqrs':
            self.qrs_idx = xqrs_detect(self.signal_raw, self.fs)
        elif detector == 'gqrs':
            self.qrs_idx = gqrs_detect(self.signal_raw, self.fs)
        elif detector == 'pantom':
            print('Trying to use a pantomkins detector!')
            self.qrs_idx = pantompkins.detect(self.signal_raw, self.fs)
        else:
            print('detector not available')

        if correct_peak:
            self.qrs_idx, self.qrs_amp = \
            tools.ecg_qrs_drs.correct_rpeaks(self.signal_filt, self.qrs_idx, self.fs)

    def calc_btb_rr(self):
        """
        Calculates RR intervals based on QRS complex detections
        --- INPUT: ---
        --- RETURN: ---
        """
        if not hasattr(self, 'qrs_idx'):
            self.qrs_detector()
        self.rr = np.diff(self.qrs_idx) * 1000/self.fs

    def calc_btb_hr(self):
        """
        Calculates heart-rate bases on RR intervals
        --- INPUT: ---
        --- RETURN: ---
        """
        if not hasattr(self, 'rr'):
            self.calc_btb_rr()
        self.btb_hr = 60*1000/self.rr

    '''
    def ecg_deliniator(self):
    """
    Delineates the ECG using the ECG wavelet deliniator
    """
            """
    Obtains ECG annotations using a Hidden Markov Model
    """
    hmm_ann = dict.fromkeys({'p_on', 'p_peak', 'p_off',
                             'qrs_on', 'qrs_off',
                             'q', 'r', 's',
                             't_on', 't_peak', 't_off'})
    #load HMM

    #run prediction

    #convert to annotation format

    return hmm_ann
        '''

    def sig_features(self, window=10, step=5, features='all', verbose=2):
        """
        Calculates signal features
        ---- PARAMETERS -----

        ---- RETURNS -----
        """
        # convert seconds to samples
        window_sample = int(window * self.fs)
        step_sample = int(step * self.fs)

        if features == 'all':
            features = list(ECG_FEATURES.keys())
 
        self.features = features 

        len_features = sum([ECG_FEATURES.get(key) for key in features])

        idxs = np.arange(0, self.len-window_sample, step_sample)
        self.idxs = idxs

        # + window is because evaluation is done at the start of the 
        # window, but I want seconds to refer to the end.
        self.seconds = (idxs/self.fs)+window

        feature_array = np.zeros((len(idxs), len_features))
        valid_data = np.zeros(len(idxs)) # This is zero when data is flat

        if not hasattr(self, 'signal_filt'):
            self.signal_raw = tools.tools.nan_interpolate(self.signal_raw) 
            self.filtering()

        times = {f:[] for f in features}
        #breakpoint()
        t0 = time.time()
        for i, idx in enumerate(idxs):
            print('\r {}%'.format(round(i/len(idxs)*100,2)), end='\r') if verbose>=2 else 0
            j = 0
            # This line is a non-zero check. If the signal is negligable, it returns 0.
            nzc_val = 1 if np.abs(self.signal_filt[idx:idx+window_sample]).sum()>1e-10 else 0
            nzc_dif = 1 if np.abs(np.diff(self.signal_filt[idx:idx+window_sample])).sum()>1e-10 else 0
            nzc = nzc_val and nzc_dif

            for f in features:
                start_time = time.time()
                
                # If the signal here is zero, we simply mark it and move on.
                # Nada to worry about.
                if nzc==0:
                    feature_array[i, j:j+ECG_FEATURES[f]] = 0
                    valid_data[i] = 0
                    j = j+ECG_FEATURES[f]
                    continue
                else:
                    valid_data[i] = 1

                if f == 'stats':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.tools.signal_stats(self.signal_filt[idx:idx+window_sample])
                elif f == 'tci':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.tci(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'tcsc':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.tcsc(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'std_exp':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.std_exp(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'mod_exp':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.mod_exp(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'mva':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.mva(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'count':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.count(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'x_time':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.x(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'bcp':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.bcp(self.signal_filt[idx:idx+window_sample])
                elif f == 'vfleak':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.vfleak(self.signal_filt[idx:idx+window_sample])
                elif f == 'spec':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.spec(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'x_freq':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.x_freq(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'bwt':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.bwt(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'li':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.li(self.signal_filt[idx:idx+window_sample], self.fs)
                elif f == 'jekova':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_time_features.jekova(self.signal_filt[idx:idx+window_sample])
                elif f == 'p_sqi':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_freq_features.pSQI(self.signal_raw[idx:idx+window_sample], self.fs)
                elif f == 'bas_sqi':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_freq_features.basSQI(self.signal_raw[idx:idx+window_sample], self.fs)
                elif f == 'snr':
                    feature_array[i, j:j+ECG_FEATURES[f]] =\
                    tools.ecg_freq_features.snr(self.signal_raw[idx:idx+window_sample], self.fs)
                else:
                    raise RuntimeError('feature not available')

                # except Exception as e:
                #     print(i, idx, j, f)
                #     print(e)

                end_time = time.time()
                times[f].append(end_time-start_time)

                j = j+ECG_FEATURES[f]

        for k in times.keys():
            times[k] = np.array(times[k])

        self.feature_array = feature_array
        self.features = features
        self.valid_data = valid_data
        ttime = time.time()-t0
        if verbose>0:
            print(f'\tTime taken for ECG features = {ttime//60:.0f}m {ttime%60:.1f}s')
        return times

    def get_dataframe(self):
        if not hasattr(self, 'feature_array'):
            self.sig_features()

        self.feature_df = pd.DataFrame()
        self.feature_df['seconds'] = self.seconds
        self.feature_df['valid_data_ecg'] = self.valid_data
        columns = []
        for f in self.features:
            columns.extend(FEATURE_GROUPS[f])
        for i,fname in enumerate(columns):
            self.feature_df[fname] = self.feature_array[:,i]

        self.feature_df = self.feature_df.rename(columns={
            c:'sigfeat_'+c
            for c in columns
            })

        return self.feature_df

    def beat_correlation(self):
        """
        Calculates correlation between individual and template beats
        """
        if not hasattr(self, 'signal_filt'):
            self.filtering()
        if not hasattr(self, 'qrs_idx'):
            self.qrs_detector()

        beats = tools.ecg_beat_features.extract_beats(self.signal_filt, self.qrs_idx, self.fs)
        self.beats = beats
        beat_templates = tools.ecg_beat_features.beat_template(beats)
        self.beat_correlations = tools.ecg_beat_features.beat_correlation(beats, beat_templates)

    def windowed_beat_correlation(self):
        pass





#################################################
#################################################
#################################################
#################################################
###                                           ###
###               ABP features                ###
###                                           ###
#################################################
#################################################
#################################################
#################################################





class BP(Signal):
    """
    TODO
    """
    def __init__(self, signal_raw, category, fs=125):
        super().__init__(signal_raw, fs, category=category.upper())
        assert category.upper() in ['ABP','PPG']
        self.meta_features = ['mean', 'range', 'jump']
        self.feats_df = pd.DataFrame({'seconds':[]})

        self.default_features = [
            'autocor_all',
            'quartiles',
            'abp_stats',

            'pressure',
            'slope',
            'smoothness',
            'hr',

            'jsqi',]
    
    def convert_beat_features(self, feats, signal, step, window):
        """
        Here we take the beat-level features that are produced by get_sunfeats() (et al), 
        along with the indicators for the beat onset times and the desired step and window 
        length. At each step, the beats in the preceding window are found, and meta-features
        produced from them.
        
        Currently, for each feature, we find the mean, range, and biggest single-beat jump

        This returns a dataframefeats_df
        """

        # The-1 is from the fact that we exclude that last beat onset marker,
        # since it is an endpoint.

        # As per the ECG class, the times and idx's indicate the END of the window.
        # Note, this means that in the ECG class, the "seconds" column is equal to 
        # idx/fs + window.
        row_times = np.arange(window, len(signal)/self.fs, step)

        # These are features which have an absolute time, rather than a relative value.
        exclude_feats = ['times_syst',
                         'times_dias',
                         'endOfSys1',
                         'endOfSys2']

        if self.category=='ppg':
            exclude_feats.extend([
                'pressure_syst',
                'pressure_dias',
                'pressure_pulse',
                'map',
                'beat_period',
                'mean_dyneg',
                'sysArea1',
                'sysArea2',
                'std',
                'jsqi',])

        keys = sorted(feats.keys()) 
        keys = [k for k in keys if k not in exclude_feats]


        dfcontent = {}
        for meta_feature in self.meta_features:
            dfcontent.update( {f'abpfeat-beat-{meta_feature}_{f}':[0]*len(row_times) for f in keys} )
        beat_feats_df = pd.DataFrame(dfcontent)
        beat_feats_df['seconds'] = row_times

        for i,time in enumerate(row_times):
            beat_indices = np.where(
                            (self.onset_times[:-1]<=time) & # [:-1] is to avoid cap point
                            (self.onset_times[:-1]>=time-window) )[0]

            if len(beat_indices)>0:
                for f in keys:
                    # Values in window
                    #breakpoint()
                    vals = feats[f][beat_indices]
                    beat_feats_df.loc[i, f'abpfeat-beat-mean_{f}']  = vals.mean()
                    beat_feats_df.loc[i, f'abpfeat-beat-range_{f}'] = vals.max()-vals.min()
                    beat_feats_df.loc[i, f'abpfeat-beat-jump_{f}']  = np.abs(np.diff(vals)).max() if len(vals)>1 else 0

            else:
                for f in keys:
                    beat_feats_df.loc[i, f'abpfeat-beat-mean_{f}']  = 0
                    beat_feats_df.loc[i, f'abpfeat-beat-range_{f}'] = 0
                    beat_feats_df.loc[i, f'abpfeat-beat-jump_{f}']  = 0

        #XX remove
        # self.dfcontent = dfcontent
        # self.row_times = row_times

        return beat_feats_df


    def get_dataframe(self):
        # A method isn't really necessary for returning a single attribute, but it
        # is included here for consistency with the ECG class.
        return self.feats_df  

    def sig_features(self, step=5, window=10, filt=0, verbose=0, features_to_get=None):
        if features_to_get is None:
            features_to_get = self.default_features

        if not hasattr(self, 'onsets'):
            self.onsets = tools.wabp.get_onsets(self.signal_raw)
            self.onset_times = self.onsets/self.fs
            
        if verbose: stime = time.time()

        if filt==0:
            sig = self.signal_raw
        else:
            self.signal_filt, self.filt_set = tools.tools.filtering(sig, self.fs)
            sig = self.signal_filt
        

        ### Get and process beat features
        beat_features = tools.abp_beat_features.get_beat_features(
            sig=sig,
            onsets=self.onsets,
            whichfeats=features_to_get)
        beat_features_df = self.convert_beat_features(
            feats=beat_features,
            signal=sig,
            step=step,
            window=window,)
        self.feats_df = self.feats_df.merge(beat_features_df, on='seconds', how='outer')

        if verbose:
            etime = time.time()
            ttime = etime-stime
            print(f'\tTime taken for ABP beat features = {ttime//60:.0f}m {ttime%60:.1f}s')
            stime = time.time()

        ### Get and process time features
        time_features_df = tools.abp_time_features.get_time_features(
            sig=sig,
            step=step,
            window=window,
            fs=self.fs,
            whichfeats=features_to_get) 
        self.feats_df = self.feats_df.merge(time_features_df, on='seconds', how='outer')

        if verbose:
            etime = time.time()
            ttime = etime-stime
            print(f'\tTime taken for ABP time features = {ttime//60:.0f}m {ttime%60:.1f}s')
        
        return self.feats_df

    def plotter(self, start_time=0, save_name='', duration=10, plot_meta=1):
        if not hasattr(self, 'onsets'):
            self.get_onsets()

        VOLTAGE = 2
        tvals = np.linspace(start_time,start_time+duration,duration*self.fs)
        clipped = self.signal_raw[start_time*self.fs:(start_time+duration)*self.fs]
        fig = plt.figure(figsize=(25,5))
        plt.plot(tvals, clipped, label='ABP')

        [plt.gca().axvline(o,color='k',linewidth=0.5) 
                                        for o in self.onsets/self.fs if (
                                            (o<= start_time+duration) and
                                            (o>= start_time)
                                            )];

        if save_name=='':
            save_name = input('File name?\n   ')
        if '.' not in save_name:
            save_name = save_name + '.png'

        if not plot_meta:
            plt.savefig(save_name,bbox_inches='tight')
            return

        ### Plot meta-features
        
        fig = plt.figure(figsize=[25, 5])

        feat = 'pressure_syst'
        beat_indices = np.where(
                    (self.onset_times[:-1]<=start_time+duration) &
                    (self.onset_times[:-1]>=start_time) )[0]

        plt.scatter(
                self.onset_times[beat_indices],
                self.beat_features[feat][beat_indices],
                marker='x',
                color='r',
                s=4,
                label='Pressure'
                )

        filtered_feats_df = self.feats_df[
                                (self.feats_df['seconds']<=start_time+duration)&
                                (self.feats_df['seconds']>=start_time-10)]

        for meta in self.meta_features:
            plt.plot(
                filtered_feats_df['seconds'],
                filtered_feats_df[f'abpfeat-beat-{meta}_{feat}'],
                marker='D',
                markersize=6,
                label=meta.title())
        
        plt.set_xlim((start_time, start_time+duration))
        plt.set_xlabel('Time [s]',fontsize=24)
        ax1.set_ylabel('Pressure [mmHg]',fontsize=24)
        plt.set_ylabel('Pressure [mmHg]',fontsize=24)

        plt.xticks(fontsize=24)
        plt.yticks(fontsize=24)
        plt.legend(prop={'size': 14})

        plt.savefig(save_name,bbox_inches='tight')



if __name__=='__main__':
    
    check = ''
    # while check.lower() not in ['y','n']:
    #     check = input('Are you sure you want to run __main__ code in SignalLab? [y/n]  ')
    # if check.lower()=='n': sys.exit()

    os.chdir('../../')

    goodun = 'a40050_000039'
    sindex = 1351394 - 125*20
    
    fs = 125
    start_time = int(sindex/fs)
    duration = 30

    files = [f for f in os.listdir() if '.txt' in f]
    
    for i, f in enumerate(files):
        if f!=goodun+'_abp.txt':
            continue

        sig = BP(np.loadtxt(f), 'ABP')
        feats_df = sig.sig_features(verbose=1)


