import numpy as np
from wfdb.processing.qrs import gqrs_detect as gqrs

def detect(sig, fs, wait=15, per_section=2, detector=None, verbose=1):
    """
    This detects qrs peaks with a time wait between peaks.  
    Used for when only a sample of intervals are needed
    """
    if wait<10 and verbose:
        print('Warning! Low values of "wait" may give overlapping results.')
        
    if detector is None:
        try:
            detector=gqrs
        except (NameError, ImportError):
            raise 'No detector given'
        
    initial_intlen_seconds = 5
    max_rounds = 4
    time_to_give_up = max_rounds*initial_intlen_seconds*fs
    waitx = wait*fs
    cursor = 0
    all_peaks = []

    if wait==0:
        return detector(sig,fs)
    
    for cursor in range(0, len(sig), waitx):
        found_peaks=[]
        interval_length = 0
        breaker=0
        while len(found_peaks)<per_section:
            interval_length += initial_intlen_seconds*fs            
            if interval_length >= time_to_give_up:
                breaker=1
                break

            interval = sig[cursor:cursor+interval_length]

            if np.isnan(interval).astype(int).sum()/len(interval)>0.1:
                breaker=1
                break
            
            interval[np.isnan(interval)] = 0
            found_peaks = detector(interval, fs)

        if breaker:
            continue

        else:
            for i in range(per_section-1):
                all_peaks.append(found_peaks[i]+cursor)
                all_peaks.append(found_peaks[i+1]+cursor)

    return all_peaks