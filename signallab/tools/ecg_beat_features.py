import numpy as np
from . import tools
from scipy import interpolate
from scipy import integrate
from pdb import set_trace as st

def extract_beats(signal, r_idx, fs, before=0.1, after=0.5):
    """
    Extracts heartbeats from an ECG signal, given a list of R-peak locations
    -----
    INPUTS:
    signal (numpy array): input signal
    r_idx (numpy array): R-wave indexs in samples
    fs (int): sampling frequency
    before (float): time before R-wave peak (sec)
    after (float): time after R-wave peak (sec)
    -----
    RETURNS:
    templates (numpy matrix): matrix containing all the beats extracted
    """

    # convert limits from seconds to samples
    BEFORE = int(before * fs)
    AFTER = int(after * fs)

    LENGTH = len(signal)

    # alocate beat matix
    beat_matix = np.zeros((len(r_idx), BEFORE+AFTER))

    # loop through all the R peaks detected
    for i in range(len(r_idx)):
        # print('\r {}%'.format(round(i/len(r_idx)*100,2)), end='\r')
        r = r_idx[i]

        # define beat limits
        b = r - BEFORE
        a = r + AFTER

        # check if beat limits fit into singal and extracts beat
        if b < 0 or a >= LENGTH:
            pass
        else:
            beat_matix[i,:] = signal[b:a]

    return beat_matix

def beat_template(beat_matrix, length=128, method='median', filter_beat=True):
    """
    Calculates beat templates
    """

    # allocate space for beat templates
    beat_template_matix = np.zeros((beat_matrix.shape[0], beat_matrix.shape[1]))

    # calculates templates
    if method =='median':
        for i in range(length, beat_matrix.shape[0]):
            beat_template_matix[i, :] = np.median(beat_matrix[i-length:i, :], axis=0)
    elif method == 'mean':
        for i in range(length, beat_matrix.shape[0]):
            beat_template_matix[i, :] = np.mean(beat_matrix[i-length:i, :], axis=0)

    # fill templates at the beginning of the signal
    beat_template_matix[:length, :] = beat_template_matix[length, :]

    return beat_template_matix

def beat_correlation(beat_matrix, beat_template_matix):
    """
    Calculates correlation between individual beats and respective beat template
    """

    # allocate vector for beat correlation results
    beat_corr = np.zeros((beat_matrix.shape[0]))

    # calculates the correlation coefficient for each beat
    for i in range(beat_matrix.shape[0]):
        if np.array_equal(beat_matrix[i,:] , beat_template_matix[i,:]):
            pass
        try:
            beat_corr[i] = np.corrcoef(beat_matrix[i,:], beat_template_matix[i,:])[0,1]
        except:
            st()

    return beat_corr

def twave_alternans(sig, r_idx, fs, before=-0.05, after=0.5):
    """
    Calculates T-wave alternans based on the spectral method
    """

    beats = extract_beats(sig, r_idx, fs, before, after)

    beats_resample, _ = tools.resampling(beats, fs, 64)

    k_score = np.zeros((beats_resample.shape[0]-128))
    k_twam = np.zeros((beats_resample.shape[0]-128))

    for b in range(128, beats_resample.shape[0]):

        x = np.zeros((beats_resample.shape[1], 128*4))

        for i in range(beats_resample.shape[1]):
            f = interpolate.interp1d(np.arange(128), beats_resample[b-128:b, i], fill_value="extrapolate")
            xnew = np.arange(0, 128, 1/4)
            x[i, :] = f(xnew)
            x[i, :] -= np.mean(x[i, :])

        pxx = np.zeros((beats_resample.shape[1], 257))
        for i in range(beats_resample.shape[1]):
            freq, pxx[i,:] = tools.power_spectrum(x[i,:], 4, window='hann')
        pxx_sum = np.sum(pxx, axis=0)

        idx_peak = np.where(freq == 0.5)
        idx_noise = np.logical_and(freq >= 0.40, freq <=0.46)

        pxx_peak = pxx_sum[idx_peak]
        pxx_noise_mean = np.mean(pxx_sum[idx_noise])
        pxx_noise_std = np.std(pxx_sum[idx_noise])

        k_score[b-128] = (pxx_peak - pxx_noise_mean) / pxx_noise_std
        if pxx_peak-pxx_noise_mean > 0:
            k_twam[b-128] = np.sqrt(pxx_peak-pxx_noise_mean)

        #plt.figure()
        #plt.plot(s.signal_filt)
        #plt.plot(freq, np.mean(pxx, axis=0))
        #plt.plot(s.qrs_idx[128:], k_score/np.max(k_score))
        #plt.plot(s.qrs_idx[128:], k_twam)
    return k_score, k_twam