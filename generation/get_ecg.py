from signallab import SignalLab

def process(config, data, fs):
    sig_obj = SignalLab.ECG(data, fs=fs)
    sig_obj.signal_filt = data
    
    window = config['ecg_window']
    step = config['step']
    
    sig_obj.sig_features(window=window, step=step)
    
    out = sig_obj.get_dataframe()
    
    return out
