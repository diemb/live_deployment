import numpy as np
import pandas as pd

def process(config, df, lookback):
    #dfout = pd.DataFrame({'seconds':df['seconds'].values})

    if np.abs(np.diff(np.diff(df['seconds'][2:-2]))).sum() > 1.0:
        print('Warning, strange behaviour in lookback features.')

    sigcols = [c for c in df.columns if 'feat' in c and 'lookback' not in c]
    mean_sigcols = [f'{sc}_LB-{lookback}-mean' for sc in sigcols]
    std_sigcols  = [f'{sc}_LB-{lookback}-std'  for sc in sigcols]

    lbrows = min(int(lookback/config['step']), len(df)-1)

    if lbrows>=len(df):
        default_df = df.copy()
        default_df.rename(columns=dict(zip(sigcols,mean_sigcols)),
                     inplace=True)
        default_df[mean_sigcols] = np.nan
        default_df[std_sigcols]  = np.nan
        return default_df


    roll = df.rolling(window=lbrows,
                      min_periods=lbrows)
    
    data_mean = roll[sigcols].mean()
    data_mean.rename(columns=dict(zip(sigcols,mean_sigcols)),
                     inplace=True)
    
    data_std = roll[sigcols].std()
    data_std.rename(columns=dict(zip(sigcols,std_sigcols)),
                     inplace=True)
    
    out = pd.concat([data_mean, data_std], axis=1)
    
    out['seconds'] = df['seconds']
    
    return out