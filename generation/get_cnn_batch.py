import numpy as np
import pandas as pd
from scipy import signal as sps

import utils
from utils import ErrMessage

feature_names_all = [f"cnnfeat_{x}" for x in range(8)]

n_points_interval = 64

def process(config, data, fs, peaks, eval_times, model=None):
    stack_size = config['stack_size']
    points_before = int(fs*0.1)
    points_after  = int(fs*0.2)

    intervals, times = utils.get_intervals(
        data,
        fs,
        peaks,
        points_before=points_before,
        points_after=points_after,
        resample_to=64 #NEEDED? XX 
        )
    intervals -= intervals.mean(axis=1).reshape(-1,1)

    # batchjump is the number along the list of intervals that we move on 
    # each iteration of the loop.
    # batchsize is how many intervals to predict on at once.
    # This is bigger than batchjump since there should be overlap, since 
    # there is a warm-up period for the cnn.
    batchjump = config.get('cnn_batch_jump', 100_000)
    batchsize = batchjump + stack_size + 2

    all_dfs = []

    startindex_vals = np.arange(0, len(intervals), batchjump)
    for j,startindex in enumerate(startindex_vals):
        print(f'\r\tCNN batch: {j+1}/{len(startindex_vals)}')
        intervals_batch = intervals[startindex:startindex + batchsize]
        times_batch     = times[startindex:startindex + batchsize]
        X = []
        for r in range(stack_size):
            print(f'\r{r+1}/{stack_size}', end='')
            X.append(np.roll(intervals_batch,-r,axis=0))
        print()
        
        X = np.stack(X) # shape = stack_size, n_intervals_in_file, n_pts_in_array
        X = np.swapaxes(X, 0, 1) # shape = n_ints_in_file, stack_size, n_pts_in_array
        X = X.reshape(*X.shape,1)
        
        # shape = n_intervals_in_file, stack_size, n_points_in_array, 1, because
        # "colour-like" channels expected

        # np.roll loops around, appending the start to the end also. We therefore
        # need to cut off the data and times at 128 before the end.
        # For the times, we chop from 128 from the beginning onwards, since we want 
        # to count the end of a block as its time.
        X = X[:-(stack_size-1)]
        times_batch = times_batch[(stack_size-1):]

        # Pass the interval stacks through the model a few at a time
        featurevecs_list = []
        batsize = config['cnn_batch_size']
        
        if len(X)==0:
            # This situation was first noticed when running the code in 
            # live generation mode. A small batch passed may result in 
            # the line: X = X[:-(stack_size-1)] removing all elements 
            # from X. In this case, all_dfs will be empty, so we also 
            # handle this case below (search tag !!lenX)
            continue

        featurevecs_list.append( model.predict(X) )
    
        if len(featurevecs_list)==0: return ErrMessage('tooshort')

        featurevecs = np.concatenate(featurevecs_list, axis=0)

        df = get_meta_vectors_from_array(config, featurevecs, times_batch, eval_times)

        all_dfs.append(df)

    #!!lenX
    # Here we can just return an empty dataframe. This is allowed due to the 
    # behaviour of remove_single_method_nan_rows from gen_all_offline.
    # All cnn nan rows would be removed anyway, so we can just return an
    # empty frame and inner join on that.
    if len(all_dfs)==0:
        return pd.DataFrame({'seconds':[]})

    combined_df = pd.concat(all_dfs)
    combined_df = combined_df.drop_duplicates(subset=['seconds'])

    # This can be length zero even if all_dfs is not empty, if we do not fill
    # out a metavector section.
    if len(combined_df)==0:
        return pd.DataFrame({'seconds':[]})

    # Mark the nan columns as invalid
    nanidx = utils.find_earliest_nonnan(combined_df, ['feat'], stack_size)
    combined_df['valid_data_cnn'] = 1
    combined_df.iloc[:nanidx]['valid_data_cnn'] = 0

    return combined_df

def get_meta_vectors_from_array(
        config,
        featurevecs,
        times_batch,
        eval_times
        ):
    eval_times_set = eval_times[np.where(
        (eval_times>=times_batch.min()) &
        (eval_times<times_batch.max())
        )[0]]
    df = pd.DataFrame({'seconds':eval_times_set}) 
    columns = [f'cnnfeat_{x:02.0f}' for x in range(featurevecs.shape[1])]
    for i_et,et in enumerate(eval_times_set):
        eval_indices = np.where((times_batch<et)&(times_batch>et-config['cnn_window']))[0]
        if len(eval_indices)==0:
            continue
        eval_vecs = featurevecs[eval_indices, :]

        df.loc[i_et, columns] = eval_vecs.mean(axis=0)
        
    return df
