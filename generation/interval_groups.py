
import numpy as np
import os
from os.path import join as  opj
from sklearn.cluster import KMeans, DBSCAN
from datetime import datetime

class IntervalGroup(object):
    def __init__(self, leads=None, data=None, modelname=None):
        self.leads = leads
        self.data  = data
        self.modelname = modelname
        self.model = None
        self.n_clusters = None

    def get_model(self, modelname, n_clusters=None):
        self._set_model(modelname=modelname, n_clusters=n_clusters)

        if modelname=='kmeans':
            assert self.n_clusters is not None
            self.model = KMeans(n_clusters=n_clusters)
        elif modelname=='dbscan':
            self.model = DBSCAN()
        else:
            raise NotImplementedError

    def _set_model(self, modelname, n_clusters=None):
        modelname = modelname.lower()
        if not ((self.modelname is None) or (self.modelname==modelname)):
            raise Exception('Trying to change model')
        if not ((self.n_clusters is None) or (self.n_clusters==n_clusters)):
            raise Exception('Trying to change n_clusters')
        else:
            self.modelname = modelname
            self.n_clusters = n_clusters

    def fit_model(self, data):
        assert self.model is not None
        assert self.modelname is not None     

        if self.modelname in ['kmeans']:
            self.transformed_train_data = self.model.fit_transform(data)
        elif self.modelname in ['dbscan']:
            raise NotImplementedError
        else:
            raise NotImplementedError
        return self.transformed_train_data

    def fit_model_internal(self):
        return self.fit_model(self.data)

    def remove_data(self):
        del(self.data)

    def transform(self, data):
        if self.modelname in ['kmeans']:
            return self.model.transform(data)
        elif self.modelname in ['cnn']:
            return self.model.predict(data)
        else:
            return NotImplementedError

    def normalize(self):
        means = self.data.mean(axis=1)
        self.data = self.data - means[:,None]
        stds = self.data.std(axis=1)
        self.data = self.data / stds.mean()