from . import get_cnn_all, get_cnn_batch, get_cnn_sample

def process(config, data, fs, peaks, eval_times, model=None):
    
    mode = config.get('cnn_mode', 'batch')
    
    if mode=='all':
        module = get_cnn_all
    if mode=='batch':
        module = get_cnn_batch
    if mode=='sample':
        module = get_cnn_sample

    return module.process(config, data, fs, peaks, eval_times, model)    