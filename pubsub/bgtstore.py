
"""
@author:  Aruna Sri T , Joseph Itopa
@Date of creation: Aug-20-2021 This file will :

    - create or delete big table instance, and table for storing patient data 
    - insert_data is used to store the received raw packet
    - update_scores is called to update the scores and timestamps list for each patientId
"""

import datetime
import sys
import pandas as pd
import json
from google.cloud import bigtable

from google.cloud.bigtable import column_family

from google.cloud.bigtable import row_filters

#Read from config file
with open('live_config.json', 'r') as fopen:
   config = json.load(fopen)
    
project_id = config.get('project_id')
instance_id = config.get('instance_id')
table_id = config.get('table_id')

client = bigtable.Client(project=project_id, admin=True)
instance = client.instance(instance_id)
table = instance.table(table_id)

def convert_time(attime):
    # Gets the time since 01/01/1970 00:00:00
    timeformat = r"%m/%d/%Y %H:%M:%S"
    nseconds = datetime.datetime.strptime(attime, timeformat).timestamp()
    return int(nseconds)
    
def insert_data(patient_data):
    """
    Function to insert a new row for patient data packet received from the api
    Rowkey : patientId#admissionId#timestamp as unique identifier
    columnFamily : patData with columns fileID, lead, fs, raw_data
    
    """
    try:
        print(' inside insert_data ',patient_data['time'])
        timestamp = datetime.datetime.utcnow()
        column_family_id = "patData"
        pkt_time = convert_time(patient_data["time"]) 
        print(' converted time ',pkt_time)
        row_key = patient_data["patientID"]+"#"+patient_data["admissionID"]+"#"+str(pkt_time)
        print('insert_data in big table',row_key)
        row = table.direct_row(row_key)
        if not patient_data["fileID"]:
            print("Value of fileID is not present in json file")
        else:
            row.set_cell(column_family_id,"fileID", patient_data["fileID"])
            print("{} committed to bigtable".format(patient_data["fileID"]))
        if not patient_data["lead"]:
            print("Value of lead is not present in json file")
        else:
            row.set_cell(column_family_id,"lead", patient_data["lead"])
            print("{} committed to bigtable".format(patient_data["lead"]))
        if not patient_data["fs"]:
            print("Value of fs is not present in json file")
        else:
            row.set_cell(column_family_id,"fs", patient_data["fs"])
            print("{} committed to bigtable".format(patient_data["fs"]))
        if not patient_data["data"]:
            print("Value of data is not present in json file")
        else:
            rawdata = (",".join([str(i) for i in patient_data['data']])).encode()
            print(' insert_data n table size of data ', len(rawdata))
            row.set_cell(column_family_id,"raw_data", rawdata)

        row.set_cell(column_family_id,"time", patient_data["time"])
    
        row.commit()
        print('Successfully wrote row {}'.format(row_key))
    except:
        print('Exception in bgtstore: insert_data:', row_key) 

def update_scores(pat_dict):
    """
    Function to update the scores after model inference with a list of timestamp, scores
    
    Rowkey : patientId#admissionId#timestamp as unique identifier
    columnFamily : patScores with json of timestamps, scores 
    
    """
    try:
        column_family_id = "patScores"
        pkt_time = convert_time(pat_dict['time']) 
        row_key = (pat_dict['patientID']+'#'+pat_dict['admissionID'] +'#'+str(pkt_time)).encode()
        print('update_scores in big table',row_key)
        row = table.direct_row(row_key)

        if not len(pat_dict['timestamps']):
            print("Empty times list")
        elif not len(pat_dict['scores']):
            print("Empty scores list")
        else:
            ddict = {
                'timestamps': pat_dict['timestamps'],
                'scores': pat_dict['scores']
            }

            dict_json = json.dumps(ddict)
            row.set_cell(column_family_id,"prediction", dict_json)
            row.commit()
            print('Successfully update scores {}'.format(row_key))
    
    except:
        print('Exception in bgtstore: updateScores:', pat_dict) 


# A function to create instance, tables, and instantiate schema 
def create_instance(instanceID):
        print("\nChecking instance status")
        if not instance.exists():
                print("\nInstance {} does not exist. Creating..".format(instanceID))
                instance.create()
                print("\nInstance successfully created..".format(instanceID))
        else:
                print("\nInstance already exist..: {}".format(instanceID))

# A function to delete instance
def delete_instance(instanceID):
        print("\nDeleting instance")
        if not instance.exists():
                print("\nInstance {} does not exist.".format(instanceID))
        else:
                instance.delete()
                print("\nInstance Deleted: {}".format(instanceID))
 #Function to create table               
def create_table(tableID):
    column_family_id = "patientInfo"
    table = instance.table(tableID)
    max_versions_rule = column_family.MaxVersionsGCRule(2)
    column_families = {column_family_id: max_versions_rule}
    if not table.exists():
        table.create(column_families=column_families)
        print("Table {} have been successfully created".format(tableID))
    else:
        print("Table {} already exists.".format(tableID))
        
#function to delete table
def delete_table(tableID):
    table = instance.table(tableID)
    table.delete()
    print("Table {} have been successfully deleted".format(tableID))
    
# delete cells from the bigtable
def delete_cell(colum_family_id, column_name):
        row_key = b'ENTER ROW KEY' # Ensure you enter row key
        row_obj = table.row(row_key)
        print("deleting cell with id {}".format(row_key))
        row_obj.delete_cell(COLUMN_FAMILY_ID=colum_family_id, COL_NAME=column_name)
        row_obj.commit()

""" USE THE COMMENTED CODES BELOW TO INSERT AND READ DATA FROM THIS SCRIPT. ALSO TO CREATE A NEW TABLE"""

## Create a new instance
# create_instance('ENTER NEW INSTANCE NAME)
## Use table id: 'patientdata-table'
# read_row('ENTER TABLE NAME')
## Enter the name of the new table
# create_table('ENTER NEW TABLE NAME')

## Enter the name of the table to be deleted
# delete_table('')
## Delete cells from column
# delete_cell(colum_family_id, column_name)