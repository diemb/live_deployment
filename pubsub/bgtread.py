"""
@author:  Aruna Sri T , Joseph Itopa Abubakar
@Date of creation: Aug-27-2021 This file will :

    - read latest data stored in bigtable
    - read all the data from bigtable using prefix
"""
import datetime

from google.cloud import bigtable

from google.cloud.bigtable import column_family

from google.cloud.bigtable import row_filters

from google.cloud.bigtable.row_set import RowSet


with open('live_config.json', 'r') as fopen:
   config = json.load(fopen)
    
project_id = config.get('project_id')
instance_id = config.get('instance_id')
table_id = config.get('table_id')

client = bigtable.Client(project=project_id, admin=True)
instance = client.instance(instance_id)
table = instance.table(table_id)


def read_prefix(prefix):
    end_key = prefix[:-1] + chr(ord(prefix[-1]) + 1)

    row_set = RowSet()
    row_set.add_row_range_from_keys(prefix.encode("utf-8"), 
                                    end_key.encode("utf-8")) 

    rows = table.read_rows(row_set=row_set)
    return rows

def print_row(row):
    print("Reading data for {}:".format(row.row_key.decode('ISO-8859–1')))
    for cf, cols in sorted(row.cells.items()):
        print("Column Family {}".format(cf))
        for col, cells in sorted(cols.items()):
            for cell in cells:
                labels = " [{}]".format(",".join(cell.labels)) \
                    if len(cell.labels) else ""
            print(
                    "\t{}: {} @{}{}".format(col.decode('ISO-8859–1'),
                                            cell.value.decode('ISO-8859–1'),
                                            cell.timestamp, labels))


def read_latest_row(tableID):
    table = instance.table(tableID)
    row_key = 'SDDB_31#0#1595582190'
    row = table.read_row(row_key)
    print_row(row)

# Read latest data stored on bigtable
# read_latest_row('ENTER TABLE ID') 

# read all data on bigtable using prefix
# data_rows = read_prefix('SDDB_31') 
# for row in data_rows:
#    print_row(row)