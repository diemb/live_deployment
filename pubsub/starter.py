"""
@author: Diem Bui
@Date of creation: Aug-02-2021
This file will :
    - create storages such as bucket, topic, subscribers, database instance and database schema once the Flask application starts
    - Publish the data to topic
    - Pull the data from topic to subscribers
    - Send the data to Bucket & clean up the subscription
"""

import sys
import os
#import hjson as json
import json
import os
#import google
from google.cloud import storage, pubsub_v1
#from google.cloud import pubsub_v1
from google.api_core import retry
import datetime;
from pubsub import bgtstore 
# import handler as hdl
# handler = hdl.Handler()

# IAM accessible key to G App Engine
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "service-account-keys.json"

def create_bucket(bucket_name, loc = "us"):
    """
    - Create a new bucket with a given name
    - Arg:
        @ bucket_name: (string): the name of new Bucket
        @ loc: (string): the location of Bucket
    - Return a new instance of google bucket
    """
    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)
    #bucket.storage_class = "Standard"
    new_bucket = storage_client.create_bucket(bucket, location = loc)
    print(
        "Created bucket successfully {} in location {}".format(
            new_bucket.name, new_bucket.location
        )
    )
    
    storage_client.close()

    return new_bucket

def is_exist_bucket(bucket_name):
    """
    - Return True of bucket_name is already created
    - Otherwise, return False
    - Arg:
        @bucket_name: (string): the name of bucket
    """
    storage_client = storage.Client()

    buckets = storage_client.list_buckets()
    bucket_names = [buc.name for buc in buckets]

    storage_client.close()

    return (bucket_name in bucket_names)

def create_topic(project_id, topic_name):
    """
    - Create a new Pub/Sub topic.
    - Arg:
        @project_id: (string) : project Id
        @topic_name: (string): name of topic created
    - Return an instance of Google topic
    """
    publisher = pubsub_v1.PublisherClient()
    
    topic_path = publisher.topic_path(project_id, topic_name)
    topic = publisher.create_topic(request={"name": topic_path})
    print(f"Created topic: {topic.name}")
    
    #publisher.close()
    
    return topic

def is_exist_topic(project_id, topic_name):
    """
    - Return True of topic_name is already created. Otherwise, return False
    - Arg:
        @project_id: (string) : project Id
        @topic_name: (string): name of topic created 
    """
    #subscriber = pubsub_v1.SubscriberClient()
    publisher = pubsub_v1.PublisherClient()

    project_path = f"projects/{project_id}"
    topics = publisher.list_topics(request={"project": project_path})
    #print(topics)
    topic_names = [top.name for top in topics]

    # publisher.close()

    return (topic_name in f"projects/{project_id}/{topic_names}")

def create_subscription(project_id, topic_id, subscription_id):
    """Create a new pull subscription on the given topic."""
    
    publisher = pubsub_v1.PublisherClient()
    subscriber = pubsub_v1.SubscriberClient()

    topic_path = publisher.topic_path(project_id, topic_id)
    subscription_path = subscriber.subscription_path(project_id, subscription_id)
    # Wrap the subscriber in a 'with' block to automatically call close() to
    # close the underlying gRPC channel when done.
    with subscriber:
        subscription = subscriber.create_subscription(
            request={"name": subscription_path, "topic": topic_path}
        )
    
    #publisher.close()
    subscriber.close()

    """    
    def callback(message):
        print(f"Received {message}.")
        message.ack()
    future = subscriber.subscribe(subscription_path, callback=callback)
    # Wrap subscriber in a 'with' block to automatically call close() when done.
    with subscriber:
        try:
            # When `timeout` is not set, result() will block indefinitely,
            # unless an exception is encountered first.
            future.result(timeout=60)
        except TimeoutError:
            future.cancel()  # Trigger the shutdown.
            future.result()  # Block until the shutdown is complete.
    # [END pubsub_subscriber_async_pull]
    # [END pubsub_quickstart_subscriber]
    """
    
    print(f"Subscription created: {subscription_path}")
    return subscription

def is_exist_subscription(project_id, topic_id , subscription_id):
    """ Return true if subscription_id in given topic is already created. 
    Otherwise, return False
    """
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_id)
    response = publisher.list_topic_subscriptions(request={"topic": topic_path})
    list_sub = [subscription for subscription in response]
    print(list_sub)

    #publisher.close()

    return (f"projects/{project_id}/subscriptions/{subscription_id}" in list_sub)

def publish_data_to_topic(project_id, topic_id, data):
    """Push json  encoded data to pubsub topic """
    
    """
    with open('web_config.json', 'r') as fopen:
        config = json.load(fopen)
    
    project_id = config.get('project_id')
    topic_name = config.get('topic_name')
    """
    publisher = pubsub_v1.PublisherClient()

    topic_path = publisher.topic_path(project_id, topic_id)
    
    future = publisher.publish(topic_path, data)
    #print("stream future {}".format(future.result()))
    print("Published data to {}.".format(topic_path))
    
    #publisher.close()

    return topic_path

def pull_data_to_subscriber(project_id, subscription_id, bucket_name, path):
    """ Pull the data from topic to subscriber
        1) Send the data to Bucket
        2) Send the data to database
        3) Cleanup the old message on the subscript after pulling out.
    """
    """
    with open('web_config.json', 'r') as fopen:
        config = json.load(fopen)
    
    project_id = config.get('project_id')
    bucket_name = config.get('bucket_name')
    topic_name = config.get('topic_name')
    subscription_storage = config.get('subscriber_1')
    subscription_processing = config.get('subscriber_2')
    """
    subscriber = pubsub_v1.SubscriberClient()

    subscription_path = subscriber.subscription_path(project_id, subscription_id)
    # close the underlying gRPC channel when done.
    with subscriber:
        # The subscriber pulls a specific number of messages. The actual
        # number of messages pulled may be smaller than max_messages.
        response = subscriber.pull(
            request={"subscription": subscription_path, "max_messages": 5},
            retry=retry.Retry(deadline=300),
        )

        ack_ids = []
        for received_message in response.received_messages:
            
            try:
                message  = received_message.message.data
                print(f"data type: {type(message)}.")
                data = json.loads(message)
                print(f"Received: {type(data)}")
                
                # send data to Bucket
                send_data_to_bucket(bucket_name, data)
                
                bgtstore.insert_data(data)
                print(f"data has been sent to database.")
                #data = json.dumps(data)
                """
                if (path == 'storage'):
                    # send data to Bucket
                    send_data_to_bucket(bucket_name, data)
                elif (path == 'database'):
                    write_data(data)
                    print(f"data has been sent to database.")
                else:
                    print(f'Path :{path} is not defined.')
               """
            except Exception as e:
                print(f"Error in pulling data from subscription {e}")
             
            
            ack_ids.append(received_message.ack_id)

        # Acknowledges the received messages so they will not be sent again.
        subscriber.acknowledge(
            request={"subscription": subscription_path, "ack_ids": ack_ids}
        )
        print(
            f"Received and acknowledged {len(response.received_messages)} messages from {subscription_path}."
        )
    
    subscriber.close()

def delete_file_from_bucket(bucket_name, filename, prefix):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blobs = list(bucket.list_blobs(prefix = prefix))
    if (len(blobs) > 0):
        for blob in blobs:
            if (blob.name == filename):
                print(f"++++++++++++ Delete {blob.name} ++++++++++++ ")
                blob.delete()
    storage_client.close()

def send_data_to_bucket(bucket_name, data):
    """
    Send the streaming data from subscriber to bucket
    """
    storage_client = storage.Client()

    bucket = storage_client.get_bucket(bucket_name)
    print("data type: {}".format(type(data)))
    #print("data: {}".format(data))
    ts = datetime.datetime.now().timestamp()
    patienID = data['patientID']
    fileID = data['fileID']
    blob_name =  str(patienID) + '-'  + str(fileID) + '-' + str(ts) + ".json"
    # blob_name = str(ts)  + '-data-blob'
    blob = bucket.blob(blob_name)  
    
    encoded_data = json.dumps(data).encode("utf-8")
    blob.upload_from_string(
        data = encoded_data,
        content_type = 'application/octet-stream',
        client = storage_client
    )
    print(
        f"Sent data to Blob {blob_name} in a Bucket {bucket_name} ."
    )

    storage_client.close()

def send_file_to_bucket(bucket_name, file, filename, prefix, delete=True):
    """
    Send the streaming data from subscriber to bucket
    """
    if (delete):
        delete_file_from_bucket(bucket_name, filename, prefix)

    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
 
    """
    blobs = list(bucket.list_blobs(prefix = prefix))
    if (len(blobs) > 0):
        for blob in blobs:
            if (blob.name == filename):
                print(f"++++++++++++ Delete {blob.name} ++++++++++++ ")
                blob.delete()
    """ 

    print("data type: {}".format(type(file)))
    blob = bucket.blob(filename)

    blob.upload_from_string(
        data = file,
        content_type = 'application/octet-stream',
        client = storage_client
    )
    print(
        f"Sent file {filename} in a Bucket {bucket_name} ."
    )

    storage_client.close()

def get_file_from_bucket(bucket_name, filename, prefix):

    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blobs = list(bucket.list_blobs(prefix = prefix))
    if (len(blobs) > 0):
        for blob in blobs:
            if (blob.name == filename):
                print("++++++ Found Blob {blob.name} ++++++ ")
                data = blob.download_as_bytes()
                storage_client.close()
                return data
    
    print (f"Retrieve patients object  from Bucket {bucket_name}")
    storage_client.close()
    return None


def pubsub_run(data):
    """
    - Publish json  encoded data to pubsub topic
    - Pull the data from topic to the subscribers 1: data storage & delete it 
    - Send the data from bucket
    """
    with open('live_config.json', 'r') as fopen:
        config = json.load(fopen)
    
    project_id = config.get('project_id')
    bucket_name = config.get('bucket_name')
    topic_name = config.get('topic_name')
    subscription_storage = config.get('subscriber_1')
    #subscription_processing = config.get('subscriber_2')

    encoded_data = json.dumps(data).encode("utf-8")    
    publish_data_to_topic(project_id, topic_name, encoded_data)
    # change is here
    pull_data_to_subscriber(project_id, subscription_storage, bucket_name, path="storage")
    #pull_data_to_subscriber(project_id, subscription_processing, None, path="database")
    
    #send_data_to_bucket(bucket_name, data)

    ## call predictive model with data
    ## predictive model returns a dataframe with patientId and risk
    #outdf = handler.update_and_predict(data)

    return "Data has been received"
    #return f"Data has been published to Topic {topic_name}, pull to Subscriber {subscription_storage} and upload to Bucket {bucket_name} "



def start():
    #print('starter----')
    # read the storage options in config file
    with open('live_config.json', 'r') as fopen:
        config = json.load(fopen)
    
    project_id = config.get('project_id')
    bucket_name = config.get('bucket_name')
    topic_name = config.get('topic_name')
    subscription_storage = config.get('subscriber_1')
    subscription_processing = config.get('subscriber_2')
    patientscache = config.get('patientscache')
    
    if(not is_exist_bucket(bucket_name)):
        create_bucket(bucket_name)
    else:
        print(f'Bucket {bucket_name} is already created')
    
    if(not is_exist_topic(project_id, topic_name)):
        create_topic(project_id, topic_name)
        #topic_id = topic.name
    else:
        print(f'Topic {topic_name} is already created')
    
    if (not is_exist_subscription(project_id, topic_name, subscription_storage)):
        create_subscription(project_id, topic_name, subscription_storage)
    else:
        print(f'Subscription {subscription_storage} is already created')
    
    if (not is_exist_subscription(project_id, topic_name, subscription_processing)):
        create_subscription(project_id, topic_name, subscription_processing)
    else:
        print(f'Subscription {subscription_processing} is already created')
  
    if(not is_exist_bucket(patientscache)):
        create_bucket(patientscache)
    else:
        print(f'Bucket {patientscache} is already created')

    print(project_id, bucket_name, topic_name, subscription_storage, subscription_processing, patientscache)

"""
if __name__=='__main__':
    
    # start()
    # print('hello')
    #### test is_exist_bucket():
    # print(is_exist_bucket("create_bucket_from_local")) # return True
    # print(is_exist_bucket("whatever")) # return False
    ### test is_exist_topic(project_id, topic_name):
    # print(is_exist_topic("transformative-deployment", "topic_ecg")) # exptect to return True
    # print(is_exist_topic("transformative-deployment", "whatever")) # exptect to return False
    
    ### test create_topic(project_id, topic_name)
    #print(create_topic("transformative-deployment", "topic_diem_test1")) # exptect to Google Topic instance
    #print(is_exist_topic("transformative-deployment", "topic_diem_test1")) # exptect to return True
    ### test  is_exist_subscription(project_id, topic_id, subscription_id):
    #print(is_exist_subscription("transformative-deployment", "topic_diem_test1", "topic_ecg-sub")) # expect to return False
    #print(is_exist_subscription("transformative-deployment", "topic_ecg", "topic_ecg-sub")) # expect to return True
"""
